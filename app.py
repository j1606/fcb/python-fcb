from flask import Flask, render_template, request, redirect, url_for


app = Flask(__name__)


# @app.route('/')
# def index():
#     return 'This is Home Page'

# @app.route('/hello')
# def hello():
#     return 'This is Hello, World Page'


# # Static Routes vs Dynamic Routes in Flask
# # Static Route
# @app.route('/Jane')
# def greet():
#     return 'Good day Jane! How are you?'

# @app.route('/John')
# def helloJohn():
#     return 'Hello John. How are you?'


# # Dynamic Route
# @app.route('/hi/<name>')
# def hi(name):
#     return f'Hi {name}. Would you like some tea?'

# Write this in your URL: http://127.0.0.1:5000/hi/John 

# @app.route('/post/<int:post_id>')
# def show_post(post_id):
#     # show the post with the given id, the id is an integer
#     return f'Post {post_id}'

# Grocery List App

groceryList = ["Chicken"]

@app.route("/", methods=["GET"])
def index():
    return render_template("index.html", groceryList=groceryList, enumerate=enumerate)



@app.route("/add_groceryItem", methods=["POST"])
def add_groceryItem():
    groceryItem = request.form["item"]
    groceryList.append(groceryItem)
    return redirect(url_for("index"))

@app.route("/delete_groceryItem/<int:item_index>", methods=["POST"])
def delete_groceryItem(item_index):
    groceryList.pop(item_index)
    return redirect(url_for("index"))


if __name__ == "__main__":
    app.run(debug=True)